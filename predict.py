import tensorflow as tf
import tensorflow.keras
import numpy as np
import time
import pandas as pd
from tensorflow.keras import backend as k
from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.layers import Activation,Dropout,Flatten,Dense
from tensorflow.python.keras.layers import Conv2D,MaxPooling2D
from tensorflow.python.keras.layers.core import Dense,Flatten
from keras.optimizers import adam
from tensorflow.python.keras.metrics import categorical_crossentropy
from tensorflow.python.keras.preprocessing.image import ImageDataGenerator
from tensorflow.python.keras.preprocessing import image
#from tensorflow.keras import *
from tensorflow.python.keras.layers.normalization import BatchNormalization
from tensorflow.python.keras.layers.convolutional import *
from matplotlib import pyplot as plt
from tensorflow.python.keras import backend as K
from sklearn.metrics import confusion_matrix
import itertools
import matplotlib.pyplot as plt
import keras
new_model = tf.keras.models.load_model('cat.h5')
img_pred = image.load_img('cat4.jpg', target_size=(150, 150))
img_pred = image.img_to_array(img_pred)
img_pred = np.expand_dims(img_pred, axis=0)
rslt = new_model.predict(img_pred)
arr = np.rint(rslt)
if(arr[0][1]==1):
    print("cat")
else:
    print("not a cat")


